package com.springmvcProject.model;

import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

public class Empl {
	
	@Min(value=1,message="age must be >=1")
	private int id;
	
	@Size(min=3,message="must be more than or equal to 3 characters")
	private String name;
	
	@Min(value=1,message="age must be >0")
	private double salary;
	
	@Size(min=1,message="required")
	private String designation;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public Empl(int id, String name, double salary, String designation) {
		super();
		this.id = id;
		this.name = name;
		this.salary = salary;
		this.designation = designation;
	}

	public Empl() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Empl [id=" + id + ", name=" + name + ", salary=" + salary + ", designation=" + designation + "]";
	}

}
